import tkinter as tk
from tkinter import *
from tkinter import filedialog
from configparser import ConfigParser
import os
import datetime
import time
import asyncio,subprocess, shlex # shlex [for create_subprocess_exe]
import threading

class database_backup():
    
    def __init__(self):
        self.root = tk.Tk()
        self.root.geometry("800x550")
        self.root.resizable(False, False)
        self.root.title("DataBase BackUp System By Ioannis Pothos - April 2023 © ")

    def app(self) :
        self.save_folder=tk.StringVar()
        self.DB = {}
        self.parser = ConfigParser()
        self.parser.read('config.ini')
        
        self.QuickOptions = LabelFrame(self.root,text = "Quick Options",padx=10,pady=5)
        self.QuickOptions.pack(ipadx=80,pady=10)
        self.button1 = tk.Button(self.QuickOptions,command = lambda : self.changeall("add"),text = "Click to Select All Databases", width=30,background='#ffffff',fg="green")
        self.button1.pack(side=LEFT)
        self.button2 = tk.Button(self.QuickOptions,text = "Click to UnSelect All Databases" ,command = lambda :self.changeall("remove"),width=30,bg='#F0F0F0',fg="red")
        self.button2.pack(side=LEFT)
        self.button3 = tk.Button(self.QuickOptions,text = "Click To Start The Backup",command = lambda : self.savedb(),fg="BLue")
        self.button3.pack(side=RIGHT)
        
        self.DataBases = LabelFrame(self.root,text = "DataBases",padx=5,pady=2)
        self.DataBases.pack(padx=2,pady=10)
        self.Label_Array = [("Option",15),
                    ("Database",15),
                    ("Last Backup",20),
                    ("Last Size [kb]",14),
                    ("Type",5),
                    ("Host",8),
                    ("Extras",8)]
        self.label_column=0
        for x,y, in self.Label_Array:
            self.title_id = tk.Button(
                self.DataBases,
                text = x,
                state = DISABLED,
                borderwidth=3,
                width=y,
                height=1
            )
            self.title_id.grid(row=0,column=self.label_column)
            self.label_column+=1
        
        Section_Counter = 1 
        for item in self.parser.sections():
            Db_Name = item
            Db_Btn = "Dd_Btn_"+str(Section_Counter)
            self.DB[item] = tk.BooleanVar()
            self.DB[item].set(False)
            self.DB[str(item+"_active")] = tk.BooleanVar()
            self.DB[str(item+"_active")].set(True)
            self.parser_items ={}
            for array in self.parser.items(item):
                self.parser_items[array[0]] = array[1]
            db_type = "PGSQL" if self.parser_items['type']== "postgresql" else "MYSQL"
            db_host= "LocalHost" if self.parser_items['host']=="localhost" else "Remote"
            db_extras = "True " if self.parser_items['extras']!="" else "False"
            db_size = str(self.parser_items['size']) if (str(self.parser_items['size'])!="" and int(float(self.parser_items['size']))>0) else "No Data"
            db_date = self.parser_items['date'] if self.parser_items['date'] !="" else "No Data"
            self.create_db_listings(Db_Btn, self.DB[item].get(), Section_Counter, 0,
                            Db_Name,db_date,db_size,db_type,db_host,db_extras)
            Section_Counter+=1
        self.db_label_counter()
        self.root.mainloop()
        
    async def timecounter(self,item):
        t= 1
        while True:
            mins, secs = divmod(t, 60)
            timer = '{:02d}:{:02d}'.format(mins, secs)
            self.DB[str(item+"_obj")]["_id"]["text"] = str("Elapsed "+timer+" s")
            await asyncio.sleep(1)
            t+=1

    async def dbcommands(self,query,db_callsign,file,extras ="",port=""):
        self.DB[str(db_callsign+"_obj")]["_id"]['relief'] = "sunken"
        self.DB[str(db_callsign+"_obj")]["_id"]['state'] = DISABLED
        self.DB[str(db_callsign+"_obj")]["_id"]["fg"] = "red"
        self.DB[db_callsign].set(False)
        self.DB[str(db_callsign+"_active")].set(False)
        self.SingleChange(db_callsign,1)
        self.db_label_counter()
        if extras !="":
            process1 =  await asyncio.create_subprocess_shell(extras)
            self.DB[str(db_callsign+"_obj")]["_id"]["text"] = "Opening Proxy"
            await asyncio.sleep(6)
        s = time.perf_counter()
        action_timestamp=datetime.datetime.now().strftime("[%d/%m/%Y][%H:%M:%S]")
        process = await asyncio.create_subprocess_shell(query)
        future = asyncio.ensure_future(self.timecounter(db_callsign))
        await process.wait()
        future.cancel()
        action_size = os.stat(file).st_size/1000
        elapsed = time.perf_counter() - s
        if extras !="":
            command = "netstat -ano | findstr :"+ str(port)
            c = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr = subprocess.PIPE)
            stdout, stderr = c.communicate()
            stdout = stdout.decode().strip().split(' ')
            stdoutfiltered = []
            for value in stdout:
                if value =="":
                    continue
                stdoutfiltered.append(value)
            pid = stdoutfiltered[stdoutfiltered.index('LISTENING')+1]
            terminatecommand = "taskkill /F /PID " +str(pid)
            self.DB[str(db_callsign+"_obj")]["_id"]["text"] = "Closing Proxy"
            await asyncio.create_subprocess_shell(terminatecommand)
            await asyncio.sleep(4)
            
        
        self.parser.set(db_callsign,"date",action_timestamp)
        self.parser.set(db_callsign,"size",str(action_size).zfill(3))
        with open('config.ini', 'w') as configfile:
            self.parser.write(configfile)
        self.DB[str(db_callsign+"_obj")]["_date"]['text'] = action_timestamp
        self.DB[str(db_callsign+"_obj")]["_size"]['text'] = action_size
        self.DB[str(db_callsign+"_obj")]["_id"]['relief'] = "raised"
        self.DB[str(db_callsign+"_obj")]["_id"]['state'] = "normal"
        self.DB[db_callsign].set(False)
        self.DB[str(db_callsign+"_active")].set(True)
        self.SingleChange(db_callsign,2)
        self.DB[str(db_callsign+"_obj")]["_id"]["text"] = "Done in "+f"{elapsed:0.2f}"+" s"
        self.db_label_counter()
        self.root.update_idletasks()
    
    def savedb(self):
        self.dbselected = False
        for item in self.parser.sections():
            if self.DB[item].get():
                self.dbselected=True
                break
        if self.dbselected==True:
            self.save_folder
            self.pick_directory= filedialog.askdirectory()
            if not self.pick_directory:
                return
            self.save_folder.set(self.pick_directory)
            for item in self.parser.sections():
                if self.DB[item].get():
                    parser_items = {}
                    extras_command = ""
                    port = ""                
                    for array in self.parser.items(item):
                        parser_items[array[0]] = array[1]
                    if parser_items['extras'] != "":
                        extras_command = parser_items['extras']
                        port = parser_items['port']
                    if parser_items['type'] == "postgresql":
                        type = "PGSQL"
                    elif parser_items['type'] == "mysql":
                        type = "MSQL"
                    g=datetime.datetime.now().strftime("[%d-%B-%Y-At-%H-%M-%S]")
                    path=self.save_folder.get()+"/"+str(item)+g+".sql"
                    if type == "PGSQL":
                        query="pg_dump --no-owner -F c --dbname=postgresql://"+str(parser_items['user'])+":"+str(parser_items['password'])+"@"+str(parser_items['host'])+":"+str(parser_items['port'])+"/"+str(parser_items['database'])+" > "+path
                    threading.Thread(target = lambda : asyncio.run(self.dbcommands(query,item,path,extras_command,port))).start()

    def SingleChange(self,item,active=0):
        self.DB[str(item+"_obj")]["_id"]['fg'] = "Green" if self.DB[item].get() else "Red"
        if active==1:
            self.DB[str(item+"_obj")]["_id"]['bg'] = "khaki2"
        if active==2:
            self.DB[str(item+"_obj")]["_id"]['bg'] = "#F0F0F0"
        self.DB[str(item+"_obj")]["_id"]['text'] = "Selected" if self.DB[item].get() else "Not Selected"
        for x in self.DB[str(item+"_obj")].keys():
            if x=="_id":
                continue
            self.DB[str(item+"_obj")][x]['fg'] = "Green" if self.DB[item].get() else "Black"
            if active==1:
                self.DB[str(item+"_obj")][x]['bg'] = "grey80"
            if active==2:
                self.DB[str(item+"_obj")][x]['bg'] = "#F0F0F0"
    
    def changeall(self,arg=""):
        if arg == "add" or arg =="remove":
            for item in self.parser.sections():
                if arg=="add" and self.DB[item].get()== False and self.DB[str(item+"_active")].get():
                    self.DB[item].set(True)
                    self.SingleChange(item)
                elif arg=="remove" and self.DB[item].get()== True and self.DB[str(item+"_active")].get():
                    self.DB[item].set(False)
                    self.SingleChange(item)
        elif arg!="":
            self.DB[arg].set(not self.DB[arg].get())
            self.SingleChange(arg)
        self.db_label_counter()

    def db_label_counter(self):
        Total_Selected = 0
        Total_Present =0
        Total_Running = 0
        for x in self.parser.sections():
            Total_Present+=1
            if self.DB[x].get():
                Total_Selected+=1
            if not self.DB[str(x+"_active")].get():
                Total_Running+=1
        self.DataBases['text'] = f"Databases Selected [ {str(Total_Selected).zfill(3)} / {str(Total_Present).zfill(3)}]  |  Databases Currently on Backup [{str(Total_Running).zfill(3)}] "

    def create_db_listings(self,id,isSelected,row,column,
                        db_name,db_date,db_size,db_type,db_host,db_extras):
        
        id = tk.Button(
            self.DataBases,
            text = "Selected" if isSelected else "Not Selected",
            fg = "Green" if isSelected else "Red",
            borderwidth=3,
            width=self.Label_Array[0][1],
            height=1,
            command = lambda : self.changeall(db_name))
        id.grid(row=row,column=column)
        
        id_name = tk.Label(
            self.DataBases,
            text = db_name,
            width=self.Label_Array[1][1]
            )
        id_name.grid(row=row,column =column+1)
        
        id_date = tk.Label(
            self.DataBases,
            text = db_date,
            width=self.Label_Array[2][1]
            )
        id_date.grid(row=row,column =column+2)
        
        id_size = tk.Label(
            self.DataBases,
            text = db_size,
            width=self.Label_Array[3][1]
            )
        id_size.grid(row=row,column =column+3)
        
        id_type = tk.Label(
            self.DataBases,
            text = db_type,
            width=self.Label_Array[4][1]
            )
        id_type.grid(row=row,column =column+4)
        
        id_host = tk.Label(
            self.DataBases,
            text = db_host,
            width=self.Label_Array[5][1]
            )
        id_host.grid(row=row,column =column+5)
        
        id_extras = tk.Label(
            self.DataBases,
            text = db_extras,
            width=self.Label_Array[6][1]
            )
        id_extras.grid(row=row,column =column+6)

        self.DB[str(db_name+"_obj")] = {}
        self.DB[str(db_name+"_obj")]["_id"] = id
        self.DB[str(db_name+"_obj")]["_name"] = id_name
        self.DB[str(db_name+"_obj")]["_date"] = id_date
        self.DB[str(db_name+"_obj")]["_size"] = id_size
        self.DB[str(db_name+"_obj")]["_type"] = id_type
        self.DB[str(db_name+"_obj")]["_host"] = id_host
        self.DB[str(db_name+"_obj")]["_extras"] = id_extras

if __name__ == '__main__':
    database_backup().app()