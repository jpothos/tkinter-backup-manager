# <p align = "center">DB Backups with Pythons Tkinter</p>

<p align ="center"><a href = "https://www.buymeacoffee.com/ipothos"> <img src= "images/bmac.png" ></a></p>

### <p align ="center">This project is also a side project that intends to automate multiple database backups with a few clicks and to save precious time and unnecessary user input.</p>
### <p align ="center">Currently supports PostgreSQL databases and PostgeSQL databases behind proxy authentication.</p>
### <p align ="center">The user must provide a "config.ini file" in the same folder as the program with the following syntax.</p>

### [Database-Callsign]
### type = postgresql
### host = <fill the host>
### database = <fill the database name>
### user = <fill the username>
### password = <fill the database password>
### port = 5432
### extras = 
### size = 
### date = 

### <p align ="center"> When the program begins, use ConfigParser() to access the credentials file and display the databases.</p>

### <p align ="center">The user then can select the Databases that he wants a backup, and the program starts the creation of backups.</p>

### <p align ="center">Each created database backup gets marked with the exact starting time and date.</p>

### <p align ="center">For those that want to use proxy authentication, the complete command should be present at extras [example:  <platform> proxy <port_a>:<port_b> <db_name>]. The program will open the port and track the PID that eventually will close when the backup completes.</p>

### <p align ="center"> In this scenario [host = localhost] and [port = <port_a>]</p>

### <p align ="center">By sharing this code, I hope to enable others to save time on similar tasks. I use it quite often, and there is a lot of space for improvement. For example to include more database types, filtering(by time), tabs(tabControl) etc...</p>
## <p align ="center">"Feel free to redistribute this code, but please make sure to mention me as the original author." </p>